# Atmospheric Pressure Logger

Simple LabVIEW datalogger for a Druck atmospheric pressure meter.

The pressure data is stored in a SQLite database and the current value is available over network using a subset of the Sartorius DCU communication protocol. A data export utility to read from the SQLite database is also available.